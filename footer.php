 <footer class="footer-style-1">
        	<div class="fake-title">
        		<h6 class="title">footer</h6>
        	</div>
        	<div class="container">
        		<div class="row">
        			<div class="col-lg-4 col-md-6">
        				<!-- Widget Start -->
        				<div class="widget widget-about">
        					<div class="widget-title mb-28">
                                <!-- <h5 class="title"><img src="images\footer-logo.png" alt="oscarthemes"></h5> -->
                                <h5 class="title">CGPSC</h5>
                            </div><!-- /Widget Title -->
        					<div class="text">
        						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloque laudantium. </p>
        					</div>
        					<ul class="social-meta style-2">
								<li><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="#"><i class="fa fa-behance"></i></a></li>
								<li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
							</ul>
        				</div>
        				<!-- Widget End -->
        			</div>
        			<div class="col-lg-4 col-md-6">
        				<!-- Widget Start -->
        				<div class="widget widget_nav_menu">
                            <div class="widget-title">
                                <h5 class="title"> Latest Exam</h5>
                            </div><!-- /Widget Title -->
                            <ul>
                                <li><a href="#">CGPSC mains exam</a></li>
                                <li><a href="#">CGPSC Prilims exam</a></li>
                                <li><a href="#">CG VYAPAM</a></li>
                            </ul><!-- / Ul -->
                        </div>
        				<!-- Widget End -->
        			</div>
        			<div class="col-lg-4 col-md-6">
        				<!-- Widget Start -->
        				<div class="widget newsletter-widget">
                            <div class="widget-title">
                                <h5 class="title"> Newsletters</h5>
                            </div><!-- /Widget Title -->
                            <div class="text">
                                <p>Subscribe Our Newsletter To Get More Update And Join Our Course Information </p>
                                <!-- Begin Mailchimp Signup Form -->
                                <div id="mc_embed_signup" class="input-field nl-form-container clearfix">
                                    <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="newsletterform validate" target="_blank" novalidate="">
                                        <input value="" type="email" name="EMAIL" id="mce-EMAIL" maxlength="32" placeholder="Enter Your Email" class="email form-control nl-email-input" required="">
                                        <div style="position: absolute; left: -5000px;">
                                            <input type="text" name="b_224c3c7777919c909b29129f9_4f4eeeae5b" tabindex="-1" value="">
                                        </div>
                                        <label class="submit-btn th-bg th-bdr"><input id="mc-embedded-subscribe" type="submit" name="subscribe" value="Subscribe"></label>
                                    </form>
                                    <div id="notification_container"></div>
                                </div>
                                <!--End mc_embed_signup-->
                            </div>
                        </div>
        				<!-- Widget End -->
        			</div>
        		</div>
        	</div>
        	<div class="copy-right text-center">
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-12">
        					<p>Copyright ©2019 <span class="th-cl">Edukul</span>. All Rights Reserved</p>
        				</div>
        			</div>
        		</div>
        	</div>
        </footer>

        <div class="modal fade" id="orangeModalSubscription" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
          aria-hidden="true">
          <div class="modal-dialog modal-notify modal-warning" role="document">
            <!--Content-->
            <div class="modal-content">
              <!--Header-->
              <div class="modal-header text-center">
                <h4 class="modal-title white-text w-100 font-weight-bold py-2">User login</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true" class="white-text">&times;</span>
                </button>
              </div>

              <!--Body-->
              <div class="modal-body">
                <div class="md-form mb-5">
                  <i class="fa fa-user prefix grey-text"></i>
                  <input type="text" id="form3" class="form-control validate">
                  <label data-error="wrong" data-success="right" for="form3">Your name</label>
                </div>

                <div class="md-form">
                  <i class="fa fa-envelope prefix grey-text"></i>
                  <input type="email" id="form2" class="form-control validate">
                  <label data-error="wrong" data-success="right" for="form2">Your email</label>
                </div>
              </div>

              <!--Footer-->
              <div class="modal-footer justify-content-center">
                <a type="button" class="btn btn-outline-warning waves-effect">Login <i class="fa fa-paper-plane-o ml-1"></i></a>
              </div>
            </div>
            <!--/.Content-->
          </div>
        </div>