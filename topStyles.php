		<link rel="apple-touch-icon" href="apple-touch-icon.html">
		<link rel="stylesheet" href="css\font-awesome.min.css">
		<link rel="stylesheet" href="css\flaticon.css">
		<!-- Plugin CSS -->
		<link rel="stylesheet" href="css\bootstrap.min.css">
		<link rel="stylesheet" href="css\animate.css">
		<link rel="stylesheet" href="css\owl.carousel.css">
		<link rel="stylesheet" href="css\owl.theme.css">
		<link rel="stylesheet" href="css\owl.transitions.css">
		<link rel="stylesheet" href="css\meanmenu.min.css">
		<link rel="stylesheet" href="css\lightbox.min.css">
		<link rel="stylesheet" href="css\magnific-popup.css">
		<!--Theme custom css -->
		<link rel="stylesheet" href="css\typography.css">
		<link rel="stylesheet" href="css\shortcode.css">
		<link rel="stylesheet" href="style.css">
		<link rel="stylesheet" href="css\color.css">
		<!--Theme Responsive css-->
		<link rel="stylesheet" href="css\responsive.css">
		<script src="js\vendor\modernizr-2.8.3-respond-1.4.2.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>