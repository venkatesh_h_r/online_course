<!DOCTYPE HTML>
<html class="no-js" lang="zxx">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="author" content="Muhammad Bilal">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="HTML, IconiqThemes Themeforest, Best Theme ">
		<!-- title here -->
		<title>CGPSC- Index</title>
		<!-- Favicon and Touch Icons -->
		<link rel="shortcut icon" href="images\fav.png">
		<?php include('topStyles.php'); ?>
	</head>
	<body>
		<?php include('header.php'); ?>
		<!-- Header End -->
        <!-- Mian Banner Start -->
        <div class="main-banner">
        	<div class="main-slider nav-style-1">
	        	<!-- Banner Slide Start -->
	        	<div class="banner-slide text-left">
	        		<figure data-overlay="8"><img src="images\banner\slide-1.jpg" alt=""></figure>
	        		<div class="banner-caption">
	        			<div class="container">
	        				<div class="small-title th-bg badge">education makes bright your future</div>
	        				<div class="title">Best Cgpsconline program at free of cost</div>
	        				<div class="description">Podcasting operational change management inside of workflows system</div>
	        				<div class="btn-wrap">
	        					<a class="btn icon-btn blue-bg" href="#">our courses</a>
	        					<a class="btn icon-btn white-bg" href="#">learn more</a>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
	        	<!-- Banner Slide End -->
	        	<!-- Banner Slide Start -->
	        	<div class="banner-slide text-center">
	        		<figure data-overlay="8"><img src="images\banner\slide-1.jpg" alt=""></figure>
	        		<div class="banner-caption">
	        			<div class="container">
	        				<div class="small-title th-bg badge">education makes bright your future</div>
	        				<div class="title">Chhattisgarh govt education tips at ur finger tips </div>
	        				<div class="description">Podcasting operational change management inside of workflows system</div>
	        				<div class="btn-wrap">
	        					<a class="btn icon-btn blue-bg" href="#">our courses</a>
	        					<a class="btn icon-btn white-bg" href="#">learn more</a>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
	        	<!-- Banner Slide End -->
	        </div>
        </div>
        <!-- Main Banner End -->
        <div class="main-content">
        	<!-- About Us Section Start -->
        	<section class="pt-130 pb-100">
        		<div class="fake-title">
	        		<h6 class="title">about</h6>
	        	</div>
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-6 col-md-12 align-self-lg-center">
        					<div class="about-caption">
        						<h6 class="title">Welcome To Our Learning Portal.</h6>
        						<p>Mr.Ajith Kumar is currently working as in Defence, civil superintendent experiances he carries the knowledge he got is sharing with all the people who required it to crack the CPTSC government exams for priliminary and mains have a detail look at the website.</p>
        						<a class="btn th-bg icon-btn" href="#">learn more</a>
        					</div>
        				</div>
        				<div class="col-lg-6 col-md-12">
        					<div class="row">
        						<div class="col-lg-6 col-md-6">
        							<!-- Services Start -->
        							<div class="services-7">
        								<span class="icon-block th-cl"><img src="images\svg\4.png" alt=""></span>
        								<h4 class="title">Learn online about CGPS</h4>
        								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuin</p>
        							</div>
        							<!-- Services End -->
        						</div>
        						<div class="col-lg-6 col-md-6">
        							<!-- Services Start -->
        							<div class="services-7">
        								<span class="icon-block th-cl"><img src="images\svg\3.png" alt=""></span>
        								<h4 class="title">Info about cgps exams </h4>
        								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuin</p>
        							</div>
        							<!-- Services End -->
        						</div>
        						<div class="col-lg-6 col-md-6">
        							<!-- Services Start -->
        							<div class="services-7">
        								<span class="icon-block th-cl"><img src="images\svg\1.png" alt=""></span>
        								<h4 class="title">Best learning materials </h4>
        								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuin</p>
        							</div>
        							<!-- Services End -->
        						</div>
        						<div class="col-lg-6 col-md-6">
        							<!-- Services Start -->
        							<div class="services-7">
        								<span class="icon-block th-cl"><img src="images\svg\2.png" alt=""></span>
        								<h4 class="title">Because of panic situation u can learn at finger tips </h4>
        								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuin</p>
        							</div>
        							<!-- Services End -->
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</section>
        	<!-- About Us Section End -->
        	<!-- Courses Section Start -->
			<section class="th-bg courses-bg pt-130 pb-100" data-overlay="04">
				<div class="fake-title">
	        		<h6 class="title">courses</h6>
	        	</div>
				<div class="container">
					<!-- Section Title Start Here -->
					<div class="section-title-2 text-center">
						<h2 class="title">Chhattisgarh Public Service Commission,<br> Raipur</h2>
					</div>
					<!-- Section Title End Here -->
					<div class="row">
						<div class="courses-slide nav-style-1">
							<div class="col-md-4 col-lg-4">
								<!--Courses Thumb Strat-->
								<div class="courses-thumb">
									<figure><img src="images\courses\img-1.jpg" alt=""></figure>
									<div class="text">
										<!-- <div class="badge-group">
											<a href="#" class="badge blue-bg">science</a>
										</div> -->
										<h6 class="title">CGPSC prilims exam</h6>
										<ul class="blog-meta courses-meta">
											<li>Advisor <a href="#"> <b class="th-cl">kalzex HG. Palx</b></a></li>
										</ul>
										<p>Magni dolores eosqui ratione voluptatem see nesciunt. Neque porro system. </p>
										<a class="btn hover-blue icon-btn" href="#">learn more</a>
									</div>
								</div>
								<!--Courses Thumb End-->
							</div>
							<div class="col-md-4 col-lg-4">
								<!--Courses Thumb Strat-->
								<div class="courses-thumb">
									<figure><img src="images\courses\img-2.jpg" alt=""></figure>
									<div class="text">
										<!-- <div class="badge-group">
											<a href="#" class="badge th-bg">business</a>
										</div> -->
										<h6 class="title">CCGPSC mains exam</h6>
										<ul class="blog-meta courses-meta">
											<li>Advisor <a href="#"> <b class="th-cl">kalzex HG. Palx</b></a></li>
										</ul>
										<p>Magni dolores eosqui ratione voluptatem see nesciunt. Neque porro system. </p>
										<a class="btn hover-blue icon-btn" href="#">learn more</a>
									</div>
								</div>
								<!--Courses Thumb End-->
							</div>
							<div class="col-md-4 col-lg-4">
								<!--Courses Thumb Strat-->
								<div class="courses-thumb">
									<figure><img src="images\courses\img-3.jpg" alt=""></figure>
									<div class="text">
<!-- 										<div class="badge-group">
											<a href="#" class="badge red-bg">english</a>
										</div> -->
										<h6 class="title">CG VYAPAM</h6>
										<ul class="blog-meta courses-meta">
											<li>Advisor <a href="#"> <b class="th-cl">kalzex HG. Palx</b></a></li>
										</ul>
										<p>Magni dolores eosqui ratione voluptatem see nesciunt. Neque porro system. </p>
										<a class="btn hover-blue icon-btn" href="#">learn more</a>
									</div>
								</div>
								<!--Courses Thumb End-->
							</div>
							<div class="col-md-4 col-lg-4">
								<!--Courses Thumb Strat-->
								<div class="courses-thumb">
									<figure><img src="images\courses\img-4.jpg" alt=""></figure>
									<div class="text">
										<!-- <div class="badge-group">
											<a href="#" class="badge blue-bg">science</a>
										</div> -->
										<h6 class="title">how to add new MCQ type questions</h6>
										<ul class="blog-meta courses-meta">
											<li>Advisor <a href="#"> <b class="th-cl">kalzex HG. Palx</b></a></li>
										</ul>
										<p>Magni dolores eosqui ratione voluptatem see nesciunt. Neque porro system. </p>
										<a class="btn hover-blue icon-btn" href="#">learn more</a>
									</div>
								</div>
								<!--Courses Thumb End-->
							</div>
							<div class="col-md-4 col-lg-4">
								<!--Courses Thumb Strat-->
								<div class="courses-thumb">
									<figure><img src="images\courses\img-3.jpg" alt=""></figure>
									<div class="text">
<!-- 										<div class="badge-group">
											<a href="#" class="badge red-bg">english</a>
										</div> -->
										<h6 class="title">how to add new discriptive type questions</h6>
										<ul class="blog-meta courses-meta">
											<li>Advisor <a href="#"> <b class="th-cl">kalzex HG. Palx</b></a></li>
										</ul>
										<p>Magni dolores eosqui ratione voluptatem see nesciunt. Neque porro system. </p>
										<a class="btn hover-blue icon-btn" href="#">learn more</a>
									</div>
								</div>
								<!--Courses Thumb End-->
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Courses Section End -->
			<!-- About Us Section Start -->
        	<section class="pt-121 pb-82 gray-bg aboutus-bg-3">
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-6 col-md-12 ">
        					<div class="about-caption">
        						<h6 class="title">Why our portal to for CGPSC </h6>
        						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur </p>
        						<div class="row">
        						<div class="col-lg-6 col-md-6">
        							<!-- Services Start -->
        							<div class="services-8 d-flex align-items-center">
        								<span class="icon-block pearpal-bg"><i class="flaticon-idea-1"></i></span>
        								<div class="overflow-text">
        								<h4 class="title">Digital Access</h4>
        								<p>Sedut perspiciatis unde omn iste natus error sites</p>
        							</div>
        							</div>
        							<!-- Services End -->
        						</div>
        						<div class="col-lg-6 col-md-6">
        							<!-- Services Start -->
        							<div class="services-8 d-flex align-items-center">
        								<span class="icon-block th-bg"><i class="flaticon-idea-1"></i></span>
        								<div class="overflow-text">
        								<h4 class="title">Expert Advisors</h4>
        								<p>Sedut perspiciatis unde omn iste natus error sites</p>
        							</div>
        							</div>
        							<!-- Services End -->
        						</div>
        						<div class="col-lg-6 col-md-6">
        							<!-- Services Start -->
        							<div class="services-8 d-flex align-items-center">
        								<span class="icon-block blue-bg"><i class="flaticon-idea-1"></i></span>
        								<div class="overflow-text">
        								<h4 class="title">Get Proper guidlines</h4>
        								<p>Sedut perspiciatis unde omn iste natus error sites</p>
        							</div>
        							</div>
        							<!-- Services End -->
        						</div>
        						<div class="col-lg-6 col-md-6">
        							<!-- Services Start -->
        							<div class="services-8 d-flex align-items-center">
        								<span class="icon-block olive-bg"><i class="flaticon-idea-1"></i></span>
        								<div class="overflow-text">
        								<h4 class="title">Email & Notification</h4>
        								<p>Sedut perspiciatis unde omn iste natus error sites</p>
        							</div>
        							</div>
        							<!-- Services End -->
        						</div>
        					</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</section>
        	<!-- About Us Section End -->
        	<!-- Event Section Start -->
			<section class="pt-130 pb-100 event-bg">
				<!-- <div class="fake-title">
	        		<h6 class="title">exams</h6>
	        	</div> -->
				<div class="container">
					<!-- Section Title Start Here -->
					<div class="section-title-2 text-center">
						<h2 class="title">Our upcoming exam <br>notification </h2>
					</div>
					<!-- Section Title End Here -->
					<div class="filterable-gallery row  masonry clear">
	                    <!--Masonry Item Start-->
	                    <div class="col-md-6 col-lg-6 mix">
	                        <!--Event Thumb Strat-->
	                        <div class="event-thumb-2">
	                            <figure><img src="images\event\event-thumb-7.jpg" alt=""></figure>
	                            <div class="text">
	                            	<div class="max-w">
		                            	<ul class="blog-meta event-meta">
											<li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
											<li><i class="fa fa-clock-o th-cl"></i>10:00 am - 01:00 pm</li>
										</ul>
		                            	<h6 class="title">How To Improve Your Computer Skills</h6>
	                            	</div>
	                            	<a class="btn icon-btn" href="#"></a>
	                            </div>
	                        </div>
	                        <!--Event Thumb End-->
	                    </div>
	                    <!--Masonry Item End -->
	                    <!--Masonry Item Start-->
	                    <div class="col-md-6 col-lg-6 mix">
	                        <!--Event Thumb Strat-->
	                        <div class="event-thumb-2">
	                            <figure><img src="images\event\event-thumb-8.jpg" alt=""></figure>
	                            <div class="text">
	                            	<div class="max-w">
		                            	<ul class="blog-meta event-meta">
											<li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
											<li><i class="fa fa-clock-o th-cl"></i>10:00 am - 01:00 pm</li>
										</ul>
		                            	<h6 class="title">How To Build An Endless Runner Game Virtual</h6>
	                            	</div>
	                            	<a class="btn icon-btn" href="#"></a>
	                            </div>
	                        </div>
	                        <!--Event Thumb End-->
	                    </div>
	                    <!--Masonry Item End -->
	                    <!--Masonry Item Start-->
	                    <div class="col-md-6 col-lg-6 mix">
	                        <!--Event Thumb Strat-->
	                        <div class="event-thumb-2">
	                            <figure><img src="images\event\event-thumb-9.jpg" alt=""></figure>
	                            <div class="text">
	                            	<div class="max-w">
		                            	<ul class="blog-meta event-meta">
											<li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
											<li><i class="fa fa-clock-o th-cl"></i>10:00 am - 01:00 pm</li>
										</ul>
		                            	<h6 class="title">UX/UI Design Meetup Version 01 In 2019</h6>
	                            	</div>
	                            	<a class="btn icon-btn" href="#"></a>
	                            </div>
	                        </div>
	                        <!--Event Thumb End-->
	                    </div>
	                    <!--Masonry Item End -->
	                </div>
	                <div class="d-flex mt-50 mb-30 justify-content-center">
	                	<a href="#" class="btn th-bg icon-btn">view more</a>
	                </div>
				</div>
			</section>
			<!-- Event Section End -->
			<!-- Staff Section Start -->
			<section class="blue-bg pt-130 team-bg pb-130">
				<div class="fake-title">
	        		<h6 class="title">Advisors</h6>
	        	</div>
				<div class="container">
					<!-- Section Title Start Here -->
					<div class="section-title-2 mb-67 white text-left">
						<div class="row d-lg-flex">
							<div class="col-lg-9 col-md-9">
								<h2 class="title m-0">Meet Our Expert <br> Advisors</h2>
							</div>
							<div class="col-lg-3 col-md-3 text-lg-right align-self-lg-center align-self-md-center">
								<a href="#" class="btn mt-sm-30 white-bg icon-btn">view more</a>
							</div>
						</div>
					</div>
					<!-- Section Title End Here -->
					<div class="row">
						<div class="nav-style-1 staff-slide">
							<div class="col-md-6 col-lg-4">
								<!-- Team Thumb Start Here -->
								<div class="team-thumb text-center">
									<figure class="blog-img">
										<img src="images\team\team-thumb-1.jpg" alt="blog">
									</figure>
									<div class="text">
		                                <h4 class="title"><a href="#">Michel DZ Thomas</a></h4>
										<p class="designation th-cl">Math Teacher</p>
										<ul class="social-meta">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-behance"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										</ul>
		                            </div>
								</div>
								<!-- Team Thumb End Here -->
							</div>
							<div class="col-md-6 col-lg-4">
								<!-- Team Thumb Start Here -->
								<div class="team-thumb text-center">
									<figure class="blog-img">
										<img src="images\team\team-thumb-2.jpg" alt="blog">
									</figure>
									<div class="text">
		                                <h4 class="title"><a href="#">Glassy PHD Model</a></h4>
										<p class="designation th-cl">Wed designer</p>
										<ul class="social-meta">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-behance"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										</ul>
		                            </div>
								</div>
								<!-- Team Thumb End Here -->
							</div>
							<div class="col-md-6 col-lg-4">
								<!-- Team Thumb Start Here -->
								<div class="team-thumb text-center">
									<figure class="blog-img">
										<img src="images\team\team-thumb-3.jpg" alt="blog">
									</figure>
									<div class="text">
		                                <h4 class="title"><a href="#">David Smile Hiksa</a></h4>
										<p class="designation th-cl">sr Engineer</p>
										<ul class="social-meta">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-behance"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										</ul>
		                            </div>
								</div>
								<!-- Team Thumb End Here -->
							</div>
							<div class="col-md-6 col-lg-4">
								<!-- Team Thumb Start Here -->
								<div class="team-thumb text-center">
									<figure class="blog-img">
										<img src="images\team\team-thumb-4.jpg" alt="blog">
									</figure>
									<div class="text">
		                                <h4 class="title"><a href="#">Sonakila Blue Bard</a></h4>
										<p class="designation th-cl">Math Teacher</p>
										<ul class="social-meta">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-behance"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										</ul>
		                            </div>
								</div>
								<!-- Team Thumb End Here -->
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Staff Section End -->
			<!-- Register Form Section Start -->
			<section class="pt-130 pb-130">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-md-12">
							<div class="register-form">
								<h6 class="title">Registration For Course Enroll</h6>
								<form id="registration-form" action="php/registration-form.php" method="POST">
                                    <div class="input-field form-group">
                                        <input class="form-control" value="" id="name" type="text" name="name" maxlength="100" placeholder="Full Name Here" data-msg-required="Please enter your name" required="">
                                            <span class="icon-box th-bg"><i class="fa fa-user"></i></span>
                                    </div>
                                    <div class="input-field form-group">
                                        <input class="form-control" value="" id="email" type="email" name="email" maxlength="100" placeholder="Your Email Address" data-msg-required="Please enter your email address" data-msg-email="Please enter a valid email address" required="">
                                            <span class="icon-box th-bg"><i class="fa fa-envelope"></i></span>
                                    </div>
                                    <div class="input-field form-group">
                                    	<select id="select-courses" name="course" data-msg-required="Please slecte 1 option" class="form-control" required="">
											<option value="Not Slected">Exam</option>
											<option value="CGPSC prilims exam">CGPSC prilims exam</option>
											<option value="CGPSC mains exam">CGPSC mains exam</option>
											<option value="CG VYAPAM">CG VYAPAM</option>
										</select>
										<span class="icon-box th-bg"><i class="fa fa-book"></i></span>
                                    </div>
                                    <div data-aos="fade-up" data-aos-delay="600" class="form-group m-0 text-left">
                                        <button class="btn th-bg icon-btn" data-loading-text="Loading..." type="submit">registration</button>
                                    </div>
                                    <div class="alert alert-success d-none alert-dismissible animated pulse fixed-alert-box" id="registrationSuccess">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button>
                                        Thanks, your message has been sent to us.
                                    </div>
                                    <div class="alert alert-danger d-none alert-dismissible animated shake fixed-alert-box" id="registrationError">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                                        </button>
                                        <strong>Error!</strong> There was an error sending your message.
                                    </div>
                                </form>
							</div>
						</div>
						<div class="col-lg-7 ml-lg-minus-3 col-md-12 custom-md-0">
							<div class="register-img-thumb">
								<figure class="float-lg-right th-bdr">
									<img src="images\img-1.jpg" alt="">
								</figure>
								<ul class="osr-countdown blue-bg countdown">
			                        <li>
			                            <span class="days">69</span>
			                            <p class="days_ref">days</p>
			                        </li>
			                        <li>
			                            <span class="hours">13</span>
			                            <p class="hours_ref">hour</p>
			                        </li>
			                        <li>
			                            <span class="minutes">44</span>
			                            <p class="minutes_ref">mint</p>
			                        </li>
			                        <li>
			                            <span class="seconds last">12</span>
			                            <p class="seconds_ref">seco</p>
			                        </li>
			                    </ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Register Form Section End -->
			<!-- Counter Section Start -->
			<!-- <section class="pb-100 ">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-6 col-sm-12 ">
							<div class="counter-style-1 text-left">
								<div class="icon-box th-cl">
									<img src="images\svg\5.png" alt="">
								</div>
								<div class="text">
								<h6 class="count-area blue-cl"> <span class="counter">569</span> M
									<sup class="counter-icon">
										<i class="fa fa-plus"></i>
									</sup>
								</h6>
								<div class="title">
									<span>Parents Reviews</span>
								</div>
							</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<div class="counter-style-1 text-left">
								<div class="icon-box th-cl">
									<img src="images\svg\6.png" alt="">
								</div>
								<div class="text">
								<h6 class="count-area blue-cl"> <span class="counter">30</span> M
									<sup class="counter-icon">
										<i class="fa fa-plus"></i>
									</sup>
								</h6>
								<div class="title">
									<span>Good Students</span>
								</div>
							</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<div class="counter-style-1 text-left">
								<div class="icon-box th-cl">
									<img src="images\svg\7.png" alt="">
								</div>
								<div class="text">
								<h6 class="count-area blue-cl"> <span class="counter">758</span> M
									<sup class="counter-icon">
										<i class="fa fa-plus"></i>
									</sup>
								</h6>
								<div class="title">
									<span>Program Awards</span>
								</div>
							</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<div class="counter-style-1 text-left">
								<div class="icon-box th-cl">
									<img src="images\svg\4.png" alt="">
								</div>
								<div class="text">
								<h6 class="count-area blue-cl"> <span class="counter">986</span> M
									<sup class="counter-icon">
										<i class="fa fa-plus"></i>
									</sup>
								</h6>
								<div class="title">
									<span>Digital Laboratory</span>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
			</section> -->
			<!-- Counter Section End -->
			<!-- Tabs Section Start -->
			<!-- Tabs Section End -->
			<!-- Blog Section Start -->
			<section class="pt-130 pb-100">
				<div class="container">
					<!-- Section Title Start Here -->
					<div class="section-title-2 text-center">
						<h2 class="title">Reads Our Latest <br> News & Blog</h2>
					</div>
					<!-- Section Title End Here -->
					<div class="row">
						<div class="col-md-6 col-lg-4">
							<!-- Blog Thumb Start Here -->
							<div class="blog-thumb">
								<figure class="blog-img">
									<img src="images\blog\blog-thumb-1.jpg" alt="blog">
								</figure>
								<div class="text">
									<ul class="blog-meta">
										<li><i class="fa fa-user th-cl"></i><a href="#">Jondy Ross</a></li>
										<li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
									</ul>
	                                <h4 class="title"><a href="#">How Screen Reader User Accesses Web Video</a></h4>
	                                <p>But must explain to you how this mistaken idea of denouncing pleasure and praising pain was born and I will give you </p>
	                                <a class="readmore-btn icon-btn" href="#">view more</a>
								</div>
							</div>
							<!-- Blog Thumb End Here -->
						</div>
						<div class="col-md-6 col-lg-4">
							<!-- Blog Thumb Start Here -->
							<div class="blog-thumb">
								<figure class="blog-img">
									<img src="images\blog\blog-thumb-2.jpg" alt="blog">
								</figure>
								<div class="text">
									<ul class="blog-meta">
										<li><i class="fa fa-user th-cl"></i><a href="#">Jondy Ross</a></li>
										<li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
									</ul>
	                                <h4 class="title"><a href="#">Web Development Update React Hook</a></h4>
	                                <p>But must explain to you how this mistaken idea of denouncing pleasure and praising pain was born and I will give you </p>
	                                <a class="readmore-btn icon-btn" href="#">view more</a>
								</div>
							</div>
							<!-- Blog Thumb End Here -->
						</div>
						<div class="col-md-6 col-lg-4 custom-d-md-none">
							<!-- Blog Thumb Start Here -->
							<div class="blog-thumb">
								<figure class="blog-img">
									<img src="images\blog\blog-thumb-3.jpg" alt="blog">
								</figure>
								<div class="text">
									<ul class="blog-meta">
										<li><i class="fa fa-user th-cl"></i><a href="#">Jondy Ross</a></li>
										<li><i class="fa fa-calendar th-cl"></i><a href="#">05 July 2019</a></li>
									</ul>
	                                <h4 class="title"><a href="#">How Screen Reader User Accesses Web Video</a></h4>
	                                <p>But must explain to you how this mistaken idea of denouncing pleasure and praising pain was born and I will give you </p>
	                                <a class="readmore-btn icon-btn" href="#">view more</a>
								</div>
							</div>
							<!-- Blog Thumb End Here -->
						</div>
					</div>
				</div>
			</section>
			<!-- Blog Section End -->
			<!-- Brand Section Start -->
			<!-- <div class="section pb-130">
				<div class="container">
					<div class="brand-slide">
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img1.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img2.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img3.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img4.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img5.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img6.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img1.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div> -->
			<!-- Brand Section End -->
        </div>
        <!-- Footer Start -->
       <?php include("footer.php"); ?>
		<!-- Footer End -->
		<!-- js file start -->
		<?php include('bottomScripts.php'); ?>
		<!-- End js file -->
	</body>

</html>