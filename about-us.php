<!DOCTYPE HTML>
<html class="no-js" lang="zxx">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="author" content="Muhammad Bilal">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="keywords" content="HTML, IconiqThemes Themeforest, Best Theme ">
		<!-- title here -->
		<title>CGPSC - Aboutus </title>
		<!-- Favicon and Touch Icons -->
		<link rel="shortcut icon" href="images\fav.png">
		<?php include('topStyles.php'); ?>
	</head>
	<body>
		<!-- LOADER --> 
        <?php include('header.php'); ?>
		<!-- Sub Banner Start Here -->
		<div class="sub-banner text-center">
			<div class="container">
				<div class="sub-banner-caption th-bg">
					<h2 class="title">About Us</h2>
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#">Home</a></li>
							<li class="breadcrumb-item active" aria-current="page">About Us</li>
						</ol>
					</nav>
				</div>
			</div>
		</div>
		<!-- Sub Banner End Here -->
        <div class="main-content">
        	<!-- About Us Section Start -->
        	<section class="pt-130 pb-130">
        	 	<div class="container">
        	 		<div class="row">
        	 			<div class="col-lg-6">
        	 				<figure class="image-thumb-3">
        	 					<img src="images\about\about-us-3.jpg" alt="">
        	 					<figcaption class="blue-bg position-absolute">
        	 						<h6 class="title">We Are Experts Learning Institution</h6>
        	 						<p>Sorrupti quos dolores et quas molestias excepturi sint </p>
        	 						<a href="#" class="readmore-btn underline-none d-flex">
        	 							<div class="play-btn th-bg"><i class="fa fa-play"></i></div><span class="align-self-center">Watch Our Latest Video</span>
        	 						</a>
        	 					</figcaption>
        	 				</figure>
        	 			</div>
        	 			<div class="col-lg-6">
        	 				<div class="text about-caption-3">
        	 					<!-- Section Title Start Here -->
								<div class="section-title-2 text-left">
									<h2 class="title">Education Makes A <br> Man Perfect</h2>
								</div>
								<!-- Section Title End Here -->
								<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui offici adeserunt mollitia animi, id est laborum et dolorumfuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore cum soluta nobis esteligendi </p>
								<p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charmplesasure the moment so blinded by desire that they </p>
								<a href="#" class="btn  th-bg icon-btn">our courses</a>
        	 				</div>
        	 			</div>
        	 		</div>
        	 	</div>
    	 	</section>
        	<!-- About Us Section End -->
        	<!-- About Us Section Start -->
        	<section class="pt-130 pb-100 services-bg">
        		<div class="container">
        			<div class="row">
						<div class="col-lg-3 col-md-6">
							<!-- Services Start -->
							<div class="services-7">
								<span class="icon-block th-cl"><i class="flaticon-idea-1"></i></span>
								<h4 class="title">Teaching In Digital Classroom</h4>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuin</p>
							</div>
							<!-- Services End -->
						</div>
						<div class="col-lg-3 col-md-6">
							<!-- Services Start -->
							<div class="services-7">
								<span class="icon-block th-cl"><i class="flaticon-idea-1"></i></span>
								<h4 class="title">We Arrange Many Events & Meetup</h4>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuin</p>
							</div>
							<!-- Services End -->
						</div>
						<div class="col-lg-3 col-md-6">
							<!-- Services Start -->
							<div class="services-7">
								<span class="icon-block th-cl"><i class="flaticon-idea-1"></i></span>
								<h4 class="title">100% Tuition Fee & No Extrea Charge</h4>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuin</p>
							</div>
							<!-- Services End -->
						</div>
						<div class="col-lg-3 col-md-6">
							<!-- Services Start -->
							<div class="services-7">
								<span class="icon-block th-cl"><i class="flaticon-idea-1"></i></span>
								<h4 class="title">Natural Compass And Playgrounds</h4>
								<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accuin</p>
							</div>
							<!-- Services End -->
						</div>
					</div>
        		</div>
        	</section>
        	<!-- About Us Section End -->
			<section class="counter-bg">
				<div class="container">
					<div class="row">
						<div class="col-lg-3 col-md-6 col-sm-12 ">
							<div class="counter-style-1 text-left">
								<div class="icon-box th-cl">
									<i class="flaticon-users"></i>
								</div>
								<div class="text">
								<h6 class="count-area"> <span class="counter">569</span> M</h6>
								<div class="title">
									<span>Parents Reviews</span>
								</div>
							</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<div class="counter-style-1 text-left">
								<div class="icon-box th-cl">
									<i class="flaticon-document"></i>
								</div>
								<div class="text">
								<h6 class="count-area"> <span class="counter">30</span>K</h6>
								<div class="title">
									<span>Good Students</span>
								</div>
							</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<div class="counter-style-1 text-left">
								<div class="icon-box th-cl">
									<i class="flaticon-medal"></i>
								</div>
								<div class="text">
								<h6 class="count-area"> <span class="counter">758</span> M</h6>
								<div class="title">
									<span>Program Awards</span>
								</div>
							</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-6 col-sm-12">
							<div class="counter-style-1 text-left">
								<div class="icon-box th-cl">
									<i class="flaticon-coffee-cup-1"></i>
								</div>
								<div class="text">
								<h6 class="count-area"> <span class="counter">986</span></h6>
								<div class="title">
									<span>Digital Laboratory</span>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Counter Section End -->
			<!-- Staff Section Start -->
			<section class="pt-130 team-bg pb-130">
				<div class="container">
					<!-- Section Title Start Here -->
					<div class="section-title-2 mb-67 text-left">
						<div class="row d-lg-flex">
							<div class="col-lg-9 col-md-9">
								<h2 class="title m-0">Meet Our Expert <br> Advisors</h2>
							</div>
							<div class="col-lg-3 col-md-3 text-lg-right align-self-lg-center align-self-md-center">
								<a href="#" class="btn mt-sm-30 th-bg icon-btn">view more</a>
							</div>
						</div>
					</div>
					<!-- Section Title End Here -->
					<div class="row">
						<div class="nav-style-1 staff-slide">
							<div class="col-md-6 col-lg-4">
								<!-- Team Thumb Start Here -->
								<div class="team-thumb text-center">
									<figure class="blog-img">
										<img src="images\team\team-thumb-1.jpg" alt="blog">
									</figure>
									<div class="text">
		                                <h4 class="title"><a href="blog-detail.html">Michel DZ Thomas</a></h4>
										<p class="designation th-cl">Math Teacher</p>
										<ul class="social-meta">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-behance"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										</ul>
		                            </div>
								</div>
								<!-- Team Thumb End Here -->
							</div>
							<div class="col-md-6 col-lg-4">
								<!-- Team Thumb Start Here -->
								<div class="team-thumb text-center">
									<figure class="blog-img">
										<img src="images\team\team-thumb-2.jpg" alt="blog">
									</figure>
									<div class="text">
		                                <h4 class="title"><a href="blog-detail.html">Glassy PHD Model</a></h4>
										<p class="designation th-cl">Wed designer</p>
										<ul class="social-meta">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-behance"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										</ul>
		                            </div>
								</div>
								<!-- Team Thumb End Here -->
							</div>
							<div class="col-md-6 col-lg-4">
								<!-- Team Thumb Start Here -->
								<div class="team-thumb text-center">
									<figure class="blog-img">
										<img src="images\team\team-thumb-3.jpg" alt="blog">
									</figure>
									<div class="text">
		                                <h4 class="title"><a href="blog-detail.html">David Smile Hiksa</a></h4>
										<p class="designation th-cl">sr Engineer</p>
										<ul class="social-meta">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-behance"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										</ul>
		                            </div>
								</div>
								<!-- Team Thumb End Here -->
							</div>
							<div class="col-md-6 col-lg-4">
								<!-- Team Thumb Start Here -->
								<div class="team-thumb text-center">
									<figure class="blog-img">
										<img src="images\team\team-thumb-4.jpg" alt="blog">
									</figure>
									<div class="text">
		                                <h4 class="title"><a href="blog-detail.html">Sonakila Blue Bard</a></h4>
										<p class="designation th-cl">Math Teacher</p>
										<ul class="social-meta">
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-behance"></i></a></li>
											<li><a href="#"><i class="fa fa-youtube"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
										</ul>
		                            </div>
								</div>
								<!-- Team Thumb End Here -->
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Staff Section End -->
			<!-- Video Section Start -->
			<section class="video-section">
				<div class="container">
					<div class="text">
						<h6 class="small-title">watch our latest video</h6>
						<h6 class="title">Let’s Take a Small Tour <br>From Our Campus</h6>
						<a href="#" class="btn icon-btn">registration</a>
						<a class="video-btn popup-youtube" href="https://www.youtube.com/embed/tgbNymZ7vqY"><span></span></a>
					</div>
				</div>
			</section>
			<!-- Video Section End -->
			<!-- Brand Section Start -->
			<div class="section pb-130 pt-130">
				<div class="container">
					<div class="brand-slide">
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img1.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img2.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img3.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img4.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img5.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img6.png" alt="">
							</div>
						</div>
						<div class="col-md-2">
							<div class="brand-thumb">
								<img src="images\brand\img1.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Brand Section End -->
        </div>
          <!-- Footer Start -->
       <?php include("footer.php"); ?>
		<!-- Footer End -->
		<!-- js file start -->
		<?php include('bottomScripts.php'); ?>
		<!-- End js file -->
	</body>

</html>