<script src="js\vendor\jquery-1.12.0.min.js"></script>
		<script src="js\plugins.js"></script>
		<script src="js\Popper.js"></script>
		<script src="js\bootstrap.min.js"></script>
		<script src="js\owl.carousel.min.js"></script>
		<script src="js\isotope.pkgd.min.js"></script>
		<script src="js\imagesloaded.pkgd.min.js"></script>
		<script src="js\scrollup.js"></script>
		<script src="js\jquery.counterup.min.js"></script>
		<script src="js\waypoints.min.js"></script>
		<script src="js\jquery.meanmenu.js"></script>
		<script src="js\lightbox.min.js"></script>
		<script src="js\jquery.magnific-popup.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcvAXp35fi4q7HXm7vcG9JMtzQbMzjRe8"></script>
        <script src="js\gmaps.js"></script>

        <!-- Contact Form Validation Js -->
        <script src="js\contact-form-validation.min.js"></script>
        <script src="js\jquery.validate.min.js"></script>


		<script src="js\main.js"></script>