<header class="header-style1-1">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-3 col-md-3 col-sm-4">
						<!-- Logo Start -->
						<!-- <h1 class="logo"><a href="index.php"><img src="images\logo.png" alt="logo"></a></h1> -->
						<h1 class="logo"><a href="index.php">CGPSC</a></h1>
						<!-- Logo End -->
					</div>
					<div class="col-lg-6 col-md-6 col-sm-8">
						<div class="d-lg-flex justify-content-lg-center">
							<div class="menu-holder">
	                            <div class="mobile-menu"></div>
	                            <div class="main-menu navigation">
	                                <nav>
	                                    <ul>
	                                        <li class="active"><a href="index.php">Home</a></li>                                                         
	                                        <li><a href="#">CGPSC exams</a>
	                                        	<ul class="sub-menu">
	                                                <li><a href="#">CGPSC prilims exam</a></li>
	                                                <li><a href="#"> CGPSC mains exam</a></li>
	                                                <li><a href="#">CG VYAPAM</a></li>
	                                            </ul>
	                                        </li>
	                                        <li><a href="#">FAQ</a></li>
	                                         <li><a href="#">Other</a>
	                                        	<ul class="sub-menu">
	                                                <li><a href="#">How to add new MCQ type questions</a></li>
	                                                <li><a href="#">How to add new discriptive type questions</a></li>
	                                                <li><a href="about-us.php">Information related to website</a></li>
	                                            </ul>
	                                        </li>
	                                        <li><a href="contact.php">Connect with us</a></li>
	                                    </ul>
	                                </nav>
	                            </div>
	                        </div>
                        </div> 
					</div>
					<div class="col-lg-3 col-md-3 hidden-xs">
						<div class="helping-box float-lg-right d-flex align-items-center">
							<!-- <div class="icon-box">
								<i class="fa fa-envelope"></i>
							</div> -->
							<div class="overflow-text d-flex">
								<a href="#" data-toggle="modal" data-target="#orangeModalSubscription" class="title pr-2">Login</a>
								<!-- <a href="#" class="title">Admin Login</a> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>